const express = require('express');
const router = express.Router();

const {
  getUserInfo
} = require('../services/usersService');

router.get('/me', async (req, res) => {
  const {userId} = req.user;
  const info = await getUserInfo(userId);
  if(info._id) {
    res.json({user: {
    '_id': info._id,
    'username': info.username,
    createdDate: info.createdDate
  }})
  } else {
    res.status(400).json({message: 'No info'})
  }
})

module.exports = {
  usersRouter: router
}