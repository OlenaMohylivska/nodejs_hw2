const express = require('express');
const router = express.Router();

const {
    getNotesByUserId,
    addNoteToUser,
    getNoteById,
    deleteNoteById,
    checkNoteById
} = require('../services/notesService');

router.get('/', async (req, res) => {
    const { userId } = req.user;
    const limit = parseInt(req.query.limit) || 0;
    const offset = parseInt(req.query.offset) || 0;
    const notes = await getNotesByUserId(userId, limit, offset);

    if(notes) {
        res.json({ 
            "offset": Number(limit) + Number(offset),
            "limit": limit,
            "count": notes.count,
            "notes": notes.notes
        })
    } else {
        res.status(400).json({message: 'Something went wrong'})
    } 
});

router.post('/', async (req, res) => {
    const { userId } = req.user;
    const isSaved = await addNoteToUser(userId, req.body.text);
   
    if(isSaved) {
        res.json({message: 'Success'});
    } else {
        res.status(400).json({message: 'Note already exist'})
    } 
});

router.get('/:id', async (req, res) => {
    const userId = req.user.userId;
    const { id } = req.params;
    const note = await getNoteById(id, userId)
    note ? res.json({note}) : res.status(400).json({message: 'Note does not exist'})
});

router.delete('/:id', async (req, res) => {
    const userId = req.user.userId;
    const { id } = req.params;
    const note = await deleteNoteById(id, userId)
    if(note) {
        res.json({message: 'Success'});
    } else {
        res.status(400).json({message: 'User does not have such a note'})
    } 
});

router.patch('/:id', async (req, res) => {
    const userId = req.user.userId;
    const { id } = req.params;
    const note = await checkNoteById(id, userId)
    if(note) {
        res.json({message: 'Success'});
    } else {
        res.status(400).json({message: 'User does not have such a note'})
    } 
});

module.exports = {
    notesRouter: router
}