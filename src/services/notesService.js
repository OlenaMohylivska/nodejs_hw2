const {Note} = require('../models/noteModel');

const getNotesByUserId = async (userId, limit = 5, skip) => {
    const count = await Note.countDocuments({userId});
    const notes = await Note.find({userId})
    .skip(skip)    
    .limit(limit);
    return {notes, count};
}

const addNoteToUser = async (userId, noteText) => {
    const checkForExist = await Note.findOne({userId, text: noteText})
    if(!checkForExist) {
        const note = new Note({text: noteText, userId});
        await note.save();
        return true
    } else {
        return false
    }
}

const getNoteById = async (id, userId) => {
  const note = await Note.findOne({_id: id, userId});
  return note;
}

const deleteNoteById = async (id, userId) => {
    const note = await Note.deleteOne({_id: id, userId});
    return note.deletedCount;
}

const checkNoteById = async (id, userId) => {
    const complete = await Note.findOne({_id: id, userId})
    const note = await Note.updateOne({_id: id, userId}, {completed: !complete.completed});
    return note;
}

module.exports = {
    getNotesByUserId,
    addNoteToUser,
    getNoteById,
    deleteNoteById,
    checkNoteById
};