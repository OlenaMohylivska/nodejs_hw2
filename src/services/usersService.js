const {User} = require('../models/userModel');

const getUserInfo = async (userId) => {
  console.log(userId);
  const userInfo = await User.findOne({_id: userId})
  return userInfo
}

module.exports = {
  getUserInfo
}