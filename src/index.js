const express = require('express');
const path = require('path');
const morgan = require('morgan')
const mongoose = require('mongoose');
const app = express();

const {notesRouter} = require('./controllers/notesController'); 
const {usersRouter} = require('./controllers/usersController'); 
const {authRouter} = require('./controllers/authController'); 
const {authMiddleware} = require('./middlewares/authMiddleware'); 

app.use(express.json());
app.use(morgan('tiny'));

app.use('/api/auth', authRouter);

app.use('/api/notes', authMiddleware, notesRouter);
app.use('/api/users', authMiddleware, usersRouter);

const start = async () => {
    try {
        await mongoose.connect('mongodb+srv://olena:12aa12aa12aa@cluster1.nwslh.mongodb.net/hw_2_notes?retryWrites=true&w=majority', {
            useNewUrlParser: true, useUnifiedTopology: true
        });
    
        app.listen(8080);
    } catch (err) {
        console.error(`Error on server startup: ${err.message}`);
    }
}

start();